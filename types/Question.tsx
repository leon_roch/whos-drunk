import type { Language } from "./Language";

export type Question = { [key in Language]: string | null };
