import { useLanguage } from "@/hooks/useLanguage";
import { useQuestions } from "@/hooks/useQuestions";
import { useVibration } from "@/hooks/useVibration";
import { ActivityIndicator, Pressable, Text, View } from "react-native";

export default function GamePage() {
  const vibrate = useVibration();
  const { currentLanguage, match } = useLanguage();
  const { question, next, loading, error } = useQuestions();

  const errorText = match({
    en: "Something went wrong, try again in a bit",
    es: "Algo salió mal, inténtalo de nuevo en un momento",
    fr: "Quelque chose s'est mal passé, réessayez dans un instant",
    de: "Etwas ist schief gelaufen, versuchen Sie es in Kürze erneut",
  });

  const tapText = match({
    en: "Tap to get a new question",
    es: "Toque para obtener una nueva pregunta",
    fr: "Appuyez pour obtenir une nouvelle question",
    de: "Tippen Sie, um eine neue Frage zu erhalten",
  });

  function handlePress() {
    vibrate("medium");
    next();
  }

  function displayQuestion() {
    if (!question) return "";
    if (question[currentLanguage] === null) next();
    return question[currentLanguage];
  }

  function displayState() {
    if (error)
      return (
        <Text className="text-center text-md font-extrabold text-rose-500">
          {errorText}
        </Text>
      );
    if (loading) return <ActivityIndicator />;
    return (
      <Text className="text-center text-xl font-extrabold text-text">
        {displayQuestion()}
      </Text>
    );
  }

  return (
    <View className="bg-bg h-full">
      <Pressable disabled={loading} onPress={handlePress}>
        <View className="flex h-[75%] justify-center items-center px-5">
          {displayState()}
        </View>
        <View className="flex h-[25%] justify-end items-center pb-10">
          <Text className="text-text text-md font-semibold">{tapText}</Text>
        </View>
      </Pressable>
    </View>
  );
}
