import { LanguageProvider } from "@/contexts/language";
import { Stack, useRouter } from "expo-router";
import { Pressable, View } from "react-native";
import AntDesign from "@expo/vector-icons/AntDesign";
import { Flag } from "@/components/flag";
import { useLanguage } from "@/hooks/useLanguage";
import { useVibration } from "@/hooks/useVibration";

export default function RootLayout() {
  const router = useRouter();
  const vibration = useVibration();

  function handleButtonPress() {
    vibration("medium");
    router.back();
  }

  function CloseButton() {
    return (
      <Pressable className="ml-5 mt-5" onPress={handleButtonPress}>
        <AntDesign name="close" size={30} color="#1e293b" />
      </Pressable>
    );
  }

  return (
    <LanguageProvider defaultLanguage="en">
      <Stack>
        <Stack.Screen
          name="index"
          options={{
            title: "",
            headerBackground: () => (
              <View className="bg-bg absolute inset-0 w-full h-full" />
            ),
            headerRight: LanguageItem,
          }}
        />
        <Stack.Screen
          name="languages"
          options={{
            title: "",
            headerBackground: () => (
              <View className="bg-bg absolute inset-0 w-full h-full" />
            ),
            animation: "slide_from_bottom",
            headerLeft: CloseButton,
          }}
        />
        <Stack.Screen
          name="game"
          options={{
            title: "",
            headerBackground: () => (
              <View className="bg-bg absolute inset-0 w-full h-full" />
            ),
            animation: "slide_from_right",
            headerRight: LanguageItem,
            headerLeft: CloseButton,
          }}
        />
      </Stack>
    </LanguageProvider>
  );
}

function LanguageItem() {
  const router = useRouter();
  const { currentLanguage } = useLanguage();
  const vibrate = useVibration();

  function handlePress() {
    vibrate("medium");
    router.push("/languages");
  }

  return (
    <Pressable className="mr-5 mt-5" onPress={handlePress}>
      <Flag language={currentLanguage} />
    </Pressable>
  );
}
