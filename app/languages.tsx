import { Flag } from "@/components/flag";
import { useLanguage } from "@/hooks/useLanguage";
import { useVibration } from "@/hooks/useVibration";
import type { Language } from "@/types/Language";
import { useRouter } from "expo-router";
import { Pressable, Text, View } from "react-native";

const languageNames: { [key in Language]: string } = {
  en: "English",
  es: "Spanish",
  fr: "French",
  de: "German",
};

export default function LanguagesPage() {
  const router = useRouter();
  const { currentLanguage, setLanguage, match } = useLanguage();
  const vibrate = useVibration();

  const languageText = match({
    en: "Languages",
    es: "Idiomas",
    fr: "Langues",
    de: "Sprachen",
  });

  const continueText = match({
    en: "Continue",
    es: "Continuar",
    fr: "Continuer",
    de: "Fortsetzen",
  });

  function handleLanguageChange(language: Language) {
    vibrate();
    setLanguage(language);
  }

  function handleContinue() {
    vibrate("medium");
    router.back();
  }

  return (
    <View className="flex flex-col bg-bg justify-between px-5">
      <View className="h-[25%] flex justify-center">
        <Text className="text-center text-2xl font-extrabold text-text">
          {languageText}
        </Text>
      </View>
      <View
        className="h-[50%]"
        style={{
          gap: 20,
        }}
      >
        {Object.entries(languageNames).map(([language, name]) => (
          <Pressable
            style={{
              gap: 20,
            }}
            className={`flex flex-row items-center p-5 rounded-xl border-2 ${currentLanguage === language ? "border-primary" : "border-secondary"}`}
            key={language}
            onPress={() => handleLanguageChange(language as Language)}
          >
            <Flag language={language as Language} />
            <Text
              className={`weight-bold text-lg ${currentLanguage === language ? "text-primary" : "text-secondary"}`}
            >
              {name}
            </Text>
          </Pressable>
        ))}
      </View>
      <View className="h-[25%] justify-end pb-10">
        <Pressable
          className="bg-primary rounded-lg py-4 px-20"
          onPress={handleContinue}
        >
          <Text className="text-center text-lg font-semibold text-bg">
            {continueText}
          </Text>
        </Pressable>
      </View>
    </View>
  );
}
