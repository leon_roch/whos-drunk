import { useLanguage } from "@/hooks/useLanguage";
import { useVibration } from "@/hooks/useVibration";
import { useRouter } from "expo-router";
import { View, Text, Pressable } from "react-native";

export default function HomePage() {
  const router = useRouter();
  const vibration = useVibration();
  const { match } = useLanguage();

  function handleButtonPress(route: string) {
    vibration("medium");
    router.push(route);
  }

  const playText = match({
    en: "Play",
    es: "Jugar",
    fr: "Jouer",
    de: "Spielen",
  });

  return (
    <View className="flex-col items-center bg-slate-50">
      <View className="flex h-[25%] justify-center">
        <Text className="text-2xl font-extrabold text-text">Who's Drunk?</Text>
      </View>
      <View className="flex h-[25%] justify-center" />
      <View className="flex justify-end h-[50%] pb-10 w-full px-5">
        <Pressable
          className="bg-primary rounded-lg py-4"
          onPress={() => handleButtonPress("/game")}
        >
          <Text className="text-center text-xl font-semibold text-bg ">
            {playText}
          </Text>
        </Pressable>
      </View>
    </View>
  );
}
