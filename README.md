# Who's Drunk

Who's Drunk is a simple expo app that allows users to play a drinking game with their friends.

> Be aware that this app is not intended to be used by anyone under the legal drinking age in their country!

## Idea

The game is fairly simple, it asks "Who's most likely to..." questions and the players point to the person they think is most likely to do the action. \
If the majority of the players point to the same person, that person has to drink. If the players are split, everyone drinks. Then the next question is asked.

## Motivation

There are many drinking games out there, but most of them just suck. They are either too complicated, too boring, or just not fun. In addition, many drinking games have a paywall to unlock all the questions.

## General conditions

The app was created as part of a mobile course. The application had to fulfill certain framework conditions, which are listed here:

**Actuator**: Vibrations are used to give users feedback when they click a button. \
**External API**: The app uses an external API to get the questions. \
**Routing**: The app uses routing to navigate between the different screens. \
**Local Storage**: The app uses local storage to save the user's settings. \
**Specific Icon**: The app uses a specific icon to make it easier to find on the user's device.

## Scope

### Use Cases

**UC01**: As a user, I want to be able to start a new game to see the first question. \
**UC02**: As a user, I want to be able to open the settings to change the game settings. \
**UC03**: As a user, I want to be able to change the language of the game to get a better understanding of the questions. \
**UC04**: As a user, I want to be able to go back to the main menu to start a new game. \
**UC05**: As a user, I want to be able to click a button to see the next question.

### Use Case Diagram

![Use Case Diagram](./docs/use-case-diagram.png)

**User**: The person who is using the app. \
**Question API**: The API that provides the questions.

### Non-Functional Requirements

#### Functionality

| Topic | Description | Measurement |
| --- | --- | --- |
| Question Variety | The app should provide a variety of questions. | The app should provide at least 50 questions. The questions should be different from each other. |

#### Reliability

| Topic | Description | Measurement |
| --- | --- | --- |
| Error Handling | The app should handle errors gracefully and show the user a meaningful error message. | The app should not crash when an error occurs. The app should show a meaningful error message to the user. |
| Data Persistence | The app should save the user's settings in local storage. | The app should save the user's settings in local storage. The settings should be loaded when the app is started. |

#### Supportability

| Topic | Description | Measurement |
| --- | --- | --- |
| Localization | The app should support multiple languages. | The app should support at least two languages. The user should be able to change the language in the settings. |

## Sketches

![Sketches](./docs/sketches.png)

### App Icon

![App Icon](./docs/app-icon.png)

## Tech Stack

The app is built with the following technologies:

- [TypeScript](https://www.typescriptlang.org/)
- [React Native](https://reactnative.dev/)
- [Expo](https://expo.dev/) (Routing, AsyncStorage, Haptics, EAS)

## Component Diagram

![Component Diagram](./docs/component-diagram.png)

## Layer Diagram

![Layer Diagram](./docs/layer-diagram.png)

## Test Concept

The app should have UI tests to ensure that the UI is working as expected.

### Environment

**Device**: Samsung Galaxy S10, Google Pixel 4 (Emulator) \
**Os**: Android 12, Android 11 (Emulator)

### Method

The UI tests are manual black box tests that are performed on the app to ensure that the UI is working as expected.

### Test Cases

| ID | Precondition | Test Steps | Expected Result |
| --- | --- | --- | --- |
| TC01 | The phone has internet access. The app is started. | The user clicks on the "Start Game" button. | The user is shown the first question. |
| TC02 | The phone has internet access. The app is started. | The user clicks on the "Settings" button. | The user is shown the settings screen. |
| TC03 | The phone has internet access. The app is started and the user is in the settings screen. | The user changes the language to German. | The app is shown in German. |
| TC04 | The phone has internet access. The app is started and the user is in the settings screen. | The user changes the language to English. | The app is shown in English. |
| TC05 | The phone has internet access. The app is started and the user is in the game screen. | The user taps on the screen. | The user is shown the next question. |
| TC06 | The phone has internet access. The app is started and the user is in the game screen. | The user taps on the back button. | The user is shown the main menu. | 

## Source Management

The source code is managed with Git and hosted on GitLab as you can see [here](https://gitlab.com/leon_roch/whos-drunk).

![Source Versioning](./docs/source-versioning.png)

## Test Protocol

The tests were all performed manually and the results were documented in a test protocol.

| ID | Tester | Date | Time | Device | Os | Test Case | Result |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 01 | Léonard Roch | 2024.07.05 | 11:20 | Samsung Galaxy S10 | Android 12 | TC01 | Passed |
| 02 | Léonard Roch | 2024.07.05 | 11:22 | Google Pixel 4 (Emulator) | Android 11 | TC01 | Passed |
| 03 | Léonard Roch | 2024.07.05 | 11:24 | Samsung Galaxy S10 | Android 12 | TC02 | Passed |
| 04 | Léonard Roch | 2024.07.05 | 11:25 | Google Pixel 4 (Emulator) | Android 11 | TC02 | Passed |
| 05 | Léonard Roch | 2024.07.05 | 11:27 | Samsung Galaxy S10 | Android 12 | TC03 | Passed |
| 06 | Léonard Roch | 2024.07.05 | 11:28 | Google Pixel 4 (Emulator) | Android 11 | TC03 | Passed |
| 07 | Léonard Roch | 2024.07.05 | 11:30 | Samsung Galaxy S10 | Android 12 | TC04 | Passed |
| 08 | Léonard Roch | 2024.07.05 | 11:31 | Google Pixel 4 (Emulator) | Android 11 | TC04 | Passed |
| 09 | Léonard Roch | 2024.07.05 | 11:33 | Samsung Galaxy S10 | Android 12 | TC05 | Passed |
| 10 | Léonard Roch | 2024.07.05 | 11:34 | Google Pixel 4 (Emulator) | Android 11 | TC05 | Passed |
| 11 | Léonard Roch | 2024.07.05 | 11:36 | Samsung Galaxy S10 | Android 12 | TC06 | Passed |
| 12 | Léonard Roch | 2024.07.05 | 11:37 | Google Pixel 4 (Emulator) | Android 11 | TC06 | Passed |

Fortunately, all tests passed and the app is ready for release.

## Reflection

### Intention

The goal of the app was to create a simple game that people can play with their friends. I wanted to create a easy to develop game but still fun to play. It should fetch the questions from an external API to provide a variety of questions. Buttons should give the user feedback when they click on them.
It should teach me how to use the expo framework and interact with hardware components like the vibration motor or the local storage. In addition, I wanted to use an external API to get the questions. 

### Realization

The app was realized as planned. But still it got more complex than I had thought. I had to develop an additional system to handle the different languages. Moreover I needed a lot of time to decide on the final design of the app. In the future I should plan more time for the design phase and take more time to design the prototype.

### Conclusion

The expo framework is a great tool to develop mobile apps. It provides a lot of features out of the box and makes it easy to develop mobile apps. The only downside of react native compared to react is that the styling is a bit more complicated. But overall I am happy with the result and I think the app is fun to play.

## Release

The app was built with expo application services. I haven't released it yet because I would have to pay for the google play store developer account. But you can download the apk file [here](https://gitlab.com/leon_roch/whos-drunk/-/releases).