import type { Question } from "@/types/Question";

interface APIResponse {
	question: string;
	translations: {
		bn: string;
		de: string;
		es: string;
		fr: string;
		hi: string;
		tl: string;
	};
}

type Rating = "PG" | "PG13" | "R";

function buildUrl(rating?: Rating) {
	return `https://api.truthordarebot.xyz/api/paranoia${
		rating ? `?rating=${rating}` : ""
	}`;
}

export async function fetchQuestion(rating?: Rating): Promise<Question> {
	try {
		const response = await fetch(buildUrl(rating), {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (!response.ok) {
			throw new Error("Failed during fetching question");
		}

		const data: APIResponse = await response.json();

		return {
			de: data.translations.de,
			en: data.question,
			es: data.translations.es,
			fr: data.translations.fr,
		};
	} catch (error) {
		throw new Error("Cannot fetch");
	}
}
