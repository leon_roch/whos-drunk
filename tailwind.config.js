const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./app/**/*.tsx", "./components/**/*.tsx"],
  theme: {
    extend: {
      colors: {
        bg: colors.slate[50],
        primary: colors.purple[600],
        text: colors.slate[800],
        secondary: colors.purple[400],
      },
    },
  },
  plugins: [],
};
