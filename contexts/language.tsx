import { useStorage } from "@/hooks/useStorage";
import type { Language } from "@/types/Language";
import { type PropsWithChildren, createContext } from "react";

type LanguageMatcher = {
  [key in Language]: string;
};

interface LanguageContextType {
  currentLanguage: Language;
  setLanguage: (language: Language) => void;
  match: (matcher: LanguageMatcher) => string;
}

export const LanguageContext = createContext<LanguageContextType>({
  currentLanguage: "en",
  setLanguage: () => {},
  match: () => "",
});

interface LanguageProviderProps {
  defaultLanguage: Language;
}

export function LanguageProvider({
  children,
  defaultLanguage,
}: PropsWithChildren<LanguageProviderProps>) {
  const { value: currentLanguage, update } = useStorage({
    key: "language",
    initial: defaultLanguage,
  });

  function match(matcher: LanguageMatcher) {
    return matcher[currentLanguage as Language];
  }

  return (
    <LanguageContext.Provider
      value={{
        currentLanguage: currentLanguage as Language,
        setLanguage: update,
        match,
      }}
    >
      {children}
    </LanguageContext.Provider>
  );
}
