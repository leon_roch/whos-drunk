import type { Language } from "@/types/Language";
import { Image } from "expo-image";
import { useState, useEffect } from "react";

interface FlagProps {
  language: Language;
}

export function Flag({ language }: FlagProps) {
  const [source, setSource] = useState<string>("");

  useEffect(() => {
    switch (language) {
      case "en":
        setSource(require("@/assets/icons/1x1/us.svg"));
        break;
      case "es":
        setSource(require("@/assets/icons/1x1/es.svg"));
        break;
      case "fr":
        setSource(require("@/assets/icons/1x1/fr.svg"));
        break;
      case "de":
        setSource(require("@/assets/icons/1x1/de.svg"));
        break;
    }
  }, [language]);

  return <Image className="w-8 h-8 rounded-md" source={source} />;
}
