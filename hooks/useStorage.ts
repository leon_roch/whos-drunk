import { useAsyncStorage } from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";

interface StorageProps {
	key: string;
	initial: string;
}

interface StorageReturn {
	value: string;
	update: (value: string) => void;
}

export function useStorage({ key, initial }: StorageProps): StorageReturn {
	const { getItem, setItem } = useAsyncStorage(key);
	const [value, setValue] = useState<string>(initial);

	useEffect(() => {
		getItem().then((value) => {
			if (value) {
				setValue(value);
				return;
			}

			setItem(initial);
		});
	});

	function update(value: string) {
		setItem(value);
		setValue(value);
	}

	return { value, update };
}
