import { LanguageContext } from "@/contexts/language";
import { useContext } from "react";

export const useLanguage = () => useContext(LanguageContext);
