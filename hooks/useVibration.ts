import { ImpactFeedbackStyle, impactAsync } from "expo-haptics";

type ImpactType = "light" | "medium" | "heavy";

export function useVibration() {
	return (impact?: ImpactType) => {
		if (impact === "heavy") impactAsync(ImpactFeedbackStyle.Heavy);
		if (impact === "medium") impactAsync(ImpactFeedbackStyle.Medium);
		impactAsync(ImpactFeedbackStyle.Light);
	};
}
