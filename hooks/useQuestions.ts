import { fetchQuestion } from "@/apis/questions";
import type { Question } from "@/types/Question";
import { useEffect, useState } from "react";

interface QuestionResponse {
	question: Question | null;
	next: () => void;
	loading: boolean;
	error: string | null;
}

export function useQuestions(): QuestionResponse {
	const [question, setQuestion] = useState<Question | null>(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState<string | null>(null);

	useEffect(() => get(), []);

	function get() {
		setLoading(true);
		setError(null);
		fetchQuestion()
			.then((question) => {
				setQuestion(question);
			})
			.catch((error) => {
				setError(error.message);
			})
			.finally(() => {
				setLoading(false);
			});
	}

	return { question, next: get, loading, error };
}
